package com.mubeen.ezlo.repo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mubeen.ezlo.model.Ezlo;
import com.mubeen.ezlo.network.EzloService;
import com.mubeen.ezlo.utils.AppConstant;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceRepository {

    private EzloService ezloService;
    private MutableLiveData<Ezlo> ezloMutableLiveData;

    public DeviceRepository() {
        ezloMutableLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        ezloService = new retrofit2.Retrofit.Builder()
                .baseUrl(AppConstant.URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EzloService.class);
    }

    public void getDevices() {
        ezloService.getDevices().enqueue(new Callback<Ezlo>() {
            @Override
            public void onResponse(Call<Ezlo> call, Response<Ezlo> response) {
                if (response.body() != null) {
                    ezloMutableLiveData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Ezlo> call, Throwable t) {
                ezloMutableLiveData.postValue(null);
            }
        });

    }

    public LiveData<Ezlo> getEzloLiveData() {
        return ezloMutableLiveData;
    }

}
