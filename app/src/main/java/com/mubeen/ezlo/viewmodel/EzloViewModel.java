package com.mubeen.ezlo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mubeen.ezlo.model.Ezlo;
import com.mubeen.ezlo.repo.DeviceRepository;

public class EzloViewModel extends AndroidViewModel {
    private DeviceRepository deviceRepository;
    private LiveData<Ezlo> ezloLiveData;

    public EzloViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        deviceRepository = new DeviceRepository();
        ezloLiveData = deviceRepository.getEzloLiveData();
    }

    public void getDevices() {
        deviceRepository.getDevices();
    }

    public LiveData<Ezlo> getEzloLiveData() {
        return ezloLiveData;
    }
}
