package com.mubeen.ezlo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Device implements Parcelable {
    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
    @SerializedName("PK_Device")
    @Expose
    private Integer pKDevice;
    @SerializedName("MacAddress")
    @Expose
    private String macAddress;
    @SerializedName("PK_DeviceType")
    @Expose
    private Integer pKDeviceType;
    @SerializedName("PK_DeviceSubType")
    @Expose
    private Integer pKDeviceSubType;
    @SerializedName("Firmware")
    @Expose
    private String firmware;
    @SerializedName("Server_Device")
    @Expose
    private String serverDevice;
    @SerializedName("Server_Event")
    @Expose
    private String serverEvent;
    @SerializedName("Server_Account")
    @Expose
    private String serverAccount;
    @SerializedName("InternalIP")
    @Expose
    private String internalIP;
    @SerializedName("LastAliveReported")
    @Expose
    private String lastAliveReported;
    @SerializedName("Platform")
    @Expose
    private String platform;
    @SerializedName("PK_Account")
    @Expose
    private Integer pKAccount;
    private String title;

    protected Device(Parcel in) {
        if (in.readByte() == 0) {
            pKDevice = null;
        } else {
            pKDevice = in.readInt();
        }
        macAddress = in.readString();
        if (in.readByte() == 0) {
            pKDeviceType = null;
        } else {
            pKDeviceType = in.readInt();
        }
        if (in.readByte() == 0) {
            pKDeviceSubType = null;
        } else {
            pKDeviceSubType = in.readInt();
        }
        firmware = in.readString();
        serverDevice = in.readString();
        serverEvent = in.readString();
        serverAccount = in.readString();
        internalIP = in.readString();
        lastAliveReported = in.readString();
        platform = in.readString();
        if (in.readByte() == 0) {
            pKAccount = null;
        } else {
            pKAccount = in.readInt();
        }
        title = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPKDevice() {
        return pKDevice;
    }

    public void setPKDevice(Integer pKDevice) {
        this.pKDevice = pKDevice;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Integer getPKDeviceType() {
        return pKDeviceType;
    }

    public void setPKDeviceType(Integer pKDeviceType) {
        this.pKDeviceType = pKDeviceType;
    }

    public Integer getPKDeviceSubType() {
        return pKDeviceSubType;
    }

    public void setPKDeviceSubType(Integer pKDeviceSubType) {
        this.pKDeviceSubType = pKDeviceSubType;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getServerDevice() {
        return serverDevice;
    }

    public void setServerDevice(String serverDevice) {
        this.serverDevice = serverDevice;
    }

    public String getServerEvent() {
        return serverEvent;
    }

    public void setServerEvent(String serverEvent) {
        this.serverEvent = serverEvent;
    }

    public String getServerAccount() {
        return serverAccount;
    }

    public void setServerAccount(String serverAccount) {
        this.serverAccount = serverAccount;
    }

    public String getInternalIP() {
        return internalIP;
    }

    public void setInternalIP(String internalIP) {
        this.internalIP = internalIP;
    }

    public String getLastAliveReported() {
        return lastAliveReported;
    }

    public void setLastAliveReported(String lastAliveReported) {
        this.lastAliveReported = lastAliveReported;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getPKAccount() {
        return pKAccount;
    }

    public void setPKAccount(Integer pKAccount) {
        this.pKAccount = pKAccount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (pKDevice == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(pKDevice);
        }
        parcel.writeString(macAddress);
        if (pKDeviceType == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(pKDeviceType);
        }
        if (pKDeviceSubType == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(pKDeviceSubType);
        }
        parcel.writeString(firmware);
        parcel.writeString(serverDevice);
        parcel.writeString(serverEvent);
        parcel.writeString(serverAccount);
        parcel.writeString(internalIP);
        parcel.writeString(lastAliveReported);
        parcel.writeString(platform);
        if (pKAccount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(pKAccount);
        }
        parcel.writeString(title);
    }
}
