package com.mubeen.ezlo.utils;

public class AppConstant {
    public static final String URL = "https://veramobile.mios.com";
    public static final String BUNDLE_PARAM_POSITION = "position";
    public static final String BUNDLE_PARAM_DEVICE = "device";
}
