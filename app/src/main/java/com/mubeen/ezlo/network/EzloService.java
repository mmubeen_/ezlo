package com.mubeen.ezlo.network;

import com.mubeen.ezlo.model.Ezlo;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EzloService {

    @GET("/test_android/items.test")
    Call<Ezlo> getDevices();
}
