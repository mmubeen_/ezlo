package com.mubeen.ezlo;

import android.app.Application;
import android.content.Context;

public class AppClass extends Application {
    private static final String TAG = AppClass.class.getName();
    public static AppClass instance;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
