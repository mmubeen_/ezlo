package com.mubeen.ezlo.view.Listeners;

import com.mubeen.ezlo.model.Device;

public interface ActivityListener {
    void showProgress();

    void hideProgress();

    void showToast(String message);

    void showDetails(int position);

    void saveChanges(int position, Device device);
}
