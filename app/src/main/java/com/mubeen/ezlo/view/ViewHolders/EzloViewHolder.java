package com.mubeen.ezlo.view.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mubeen.ezlo.R;

public class EzloViewHolder extends RecyclerView.ViewHolder {
    public TextView titleTv;
    public TextView snTv;
    public ImageView deviceImg;
    public ImageView nextBtn;

    public EzloViewHolder(@NonNull View itemView) {
        super(itemView);

        titleTv = itemView.findViewById(R.id.item_title_tv);
        snTv = itemView.findViewById(R.id.sn_tv);
        deviceImg = itemView.findViewById(R.id.device_img);
        nextBtn = itemView.findViewById(R.id.next_btn);

    }
}
