package com.mubeen.ezlo.view.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.mubeen.ezlo.R;
import com.mubeen.ezlo.databinding.ActivityMainBinding;
import com.mubeen.ezlo.model.Device;
import com.mubeen.ezlo.view.Fragments.FragmentItemDetails;
import com.mubeen.ezlo.view.Fragments.FragmentItems;
import com.mubeen.ezlo.view.Fragments.FragmentName;
import com.mubeen.ezlo.view.Listeners.ActivityListener;
import com.mubeen.ezlo.view.Listeners.FragmentHandler;

import static com.mubeen.ezlo.utils.AppConstant.BUNDLE_PARAM_DEVICE;
import static com.mubeen.ezlo.utils.AppConstant.BUNDLE_PARAM_POSITION;
import static com.mubeen.ezlo.view.Fragments.FragmentName.FRAGMENT_ITEMS;
import static com.mubeen.ezlo.view.Fragments.FragmentName.FRAGMENT_ITEM_DETAILS;

public class MainActivity extends AppCompatActivity implements FragmentHandler {

    private ActivityMainBinding binding;
    private ProgressDialog progressDialog;
    private FragmentTransaction transaction;
    private FragmentItems fragmentItems;
    private FragmentItemDetails fragmentItemDetails;
    private ActivityListener activityListener = new ActivityListener() {

        @Override
        public void saveChanges(int position, Device device) {
            fragmentItems.getEzloViewModel().getEzloLiveData().getValue().getDevices().get(position).setTitle(device.getTitle());
            onBackPressed();
        }

        @Override
        public void showDetails(int position) {
            Bundle bundle = new Bundle(1);
            bundle.putInt(BUNDLE_PARAM_POSITION, position);
            bundle.putParcelable(BUNDLE_PARAM_DEVICE, fragmentItems.getEzloViewModel().getEzloLiveData().getValue().getDevices().get(position));
            shiftFragment(FRAGMENT_ITEM_DETAILS, bundle);
        }

        @Override
        public void showProgress() {
            if (progressDialog != null) {
                progressDialog.show();
            }
        }

        @Override
        public void hideProgress() {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void showToast(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initUI();

        shiftFragment(FRAGMENT_ITEMS);
    }

    private void initUI() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching data...");
        progressDialog.setCancelable(false);
    }

    @Override
    public void shiftFragment(FragmentName fragmentName) {
        shiftFragment(fragmentName, null);
    }

    @Override
    public void shiftFragment(FragmentName fragment_id, Bundle bundle) {
        switch (fragment_id) {

            case FRAGMENT_ITEMS:
                if (fragmentItems == null) {
                    fragmentItems = new FragmentItems(activityListener);
                }
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.fragment_container, fragmentItems);
                transaction.commit();
                break;

            case FRAGMENT_ITEM_DETAILS:
                if (fragmentItemDetails == null) {
                    fragmentItemDetails = new FragmentItemDetails(activityListener);
                }
                if (bundle != null)
                    fragmentItemDetails.setArguments(bundle);
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragmentItemDetails);
                transaction.addToBackStack(FRAGMENT_ITEM_DETAILS.name());
                transaction.commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}