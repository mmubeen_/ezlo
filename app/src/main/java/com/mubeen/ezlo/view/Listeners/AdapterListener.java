package com.mubeen.ezlo.view.Listeners;

public interface AdapterListener {
    void onItemLongClickPressed(int position);

    void onNextBtnClickPressed(int position);
}
