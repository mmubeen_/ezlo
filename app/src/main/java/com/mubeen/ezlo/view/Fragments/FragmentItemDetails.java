package com.mubeen.ezlo.view.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.mubeen.ezlo.R;
import com.mubeen.ezlo.databinding.FragmentItemDetailsBinding;
import com.mubeen.ezlo.model.Device;
import com.mubeen.ezlo.view.Listeners.ActivityListener;

import static com.mubeen.ezlo.utils.AppConstant.BUNDLE_PARAM_DEVICE;

public class FragmentItemDetails extends Fragment {
    private FragmentItemDetailsBinding binding;
    private Device device;
    private int position;
    private ActivityListener activityListener;

    public FragmentItemDetails(ActivityListener activityListener) {
        this.activityListener = activityListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            device = bundle.getParcelable(BUNDLE_PARAM_DEVICE);
            position = bundle.getInt("position");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_details, container, false);
        View view = binding.getRoot();

        switch (device.getPlatform()) {
            case "Sercomm G450":
                binding.deviceImgView.setImageDrawable(getContext().getDrawable(R.drawable.vera_plus_big));
                break;
            case "Sercomm G550":
                binding.deviceImgView.setImageDrawable(getContext().getDrawable(R.drawable.vera_secure_big));
                break;
            default:
                binding.deviceImgView.setImageDrawable(getContext().getDrawable(R.drawable.vera_edge_big));
                break;
        }


        binding.titleEdittext.setText(device.getTitle());
        binding.snTextView.setText("SN: " + device.getPKDevice());
        binding.macTview.setText("MAC: " + device.getMacAddress());
        binding.firmwareTview.setText("Firmware: " + device.getFirmware());
        binding.modelTview.setText("Model: " + device.getPlatform());

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableEditMode();
            }
        });

        binding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(binding.titleEdittext.getText()) || binding.titleEdittext.getText().length() < 4) {
                    activityListener.showToast("Title must be minimum 4 characters long");
                    return;
                }
                device.setTitle(binding.titleEdittext.getText().toString());
                activityListener.saveChanges(position, device);
            }
        });

        return view;
    }

    private void enableEditMode() {
        binding.saveBtn.setVisibility(View.VISIBLE);
        binding.editBtn.setVisibility(View.GONE);
        binding.titleEdittext.setBackground(getContext().getResources().getDrawable(R.drawable.edittext_bg));
        binding.titleEdittext.setPadding(15, 15, 15, 15);
        binding.titleEdittext.setCursorVisible(true);
        binding.titleEdittext.setFocusableInTouchMode(true);
        binding.titleEdittext.setInputType(InputType.TYPE_CLASS_TEXT);
        binding.titleEdittext.requestFocus(); //to trigger the soft input
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
