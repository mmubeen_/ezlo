package com.mubeen.ezlo.view.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mubeen.ezlo.AppClass;
import com.mubeen.ezlo.R;
import com.mubeen.ezlo.model.Device;
import com.mubeen.ezlo.view.Listeners.AdapterListener;
import com.mubeen.ezlo.view.ViewHolders.EzloViewHolder;

import java.util.ArrayList;
import java.util.List;

public class EzloAdapter extends RecyclerView.Adapter<EzloViewHolder> {
    private List<Device> deviceList = new ArrayList<>();
    private AdapterListener adapterListener;

    public EzloAdapter(AdapterListener adapterListener) {
        this.adapterListener = adapterListener;
    }


    @NonNull
    @Override
    public EzloViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_device_item, parent, false);

        return new EzloViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EzloViewHolder holder, int position) {
        Device device = deviceList.get(position);
        holder.titleTv.setText(device.getTitle());
        holder.snTv.setText("SN: " + device.getPKDevice());
        switch (device.getPlatform()) {
            case "Sercomm G450":
                holder.deviceImg.setImageDrawable(AppClass.getContext().getDrawable(R.drawable.vera_plus_big));
                break;
            case "Sercomm G550":
                holder.deviceImg.setImageDrawable(AppClass.getContext().getDrawable(R.drawable.vera_secure_big));
                break;
            default:
                holder.deviceImg.setImageDrawable(AppClass.getContext().getDrawable(R.drawable.vera_edge_big));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterListener.onNextBtnClickPressed(position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                adapterListener.onItemLongClickPressed(position);
                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
        for (int i = 0; i < deviceList.size(); i++) {
            deviceList.get(i).setTitle("Home Number " + (i + 1));
        }
        notifyDataSetChanged();
    }
}
