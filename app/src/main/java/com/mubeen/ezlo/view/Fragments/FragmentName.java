package com.mubeen.ezlo.view.Fragments;

public enum FragmentName {
    FRAGMENT_ITEMS,
    FRAGMENT_ITEM_DETAILS
}
