package com.mubeen.ezlo.view.Listeners;


import android.os.Bundle;

import com.mubeen.ezlo.view.Fragments.FragmentName;

public interface FragmentHandler {
    public void shiftFragment(FragmentName name);

    public void shiftFragment(FragmentName fragment_id, Bundle bundle);
}
