package com.mubeen.ezlo.view.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mubeen.ezlo.AppClass;
import com.mubeen.ezlo.R;
import com.mubeen.ezlo.databinding.FragmentItemsBinding;
import com.mubeen.ezlo.model.Ezlo;
import com.mubeen.ezlo.view.Adapters.EzloAdapter;
import com.mubeen.ezlo.view.Listeners.ActivityListener;
import com.mubeen.ezlo.view.Listeners.AdapterListener;
import com.mubeen.ezlo.viewmodel.EzloViewModel;

public class FragmentItems extends Fragment {
    private FragmentItemsBinding binding;
    private EzloViewModel ezloViewModel;
    private EzloAdapter ezloAdapter;
    private ActivityListener activityListener;
    public AdapterListener adapterListener = new AdapterListener() {
        @Override
        public void onNextBtnClickPressed(int position) {
            activityListener.showDetails(position);
        }

        @Override
        public void onItemLongClickPressed(int position) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Do you want to delete?");
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ezloAdapter.notifyItemRemoved(position);
                            ezloAdapter.notifyItemRangeChanged(position, ezloAdapter.getDeviceList().size());
                            ezloAdapter.notifyDataSetChanged();
                            ezloViewModel.getEzloLiveData().getValue().getDevices().remove(position);
                            activityListener.showToast("Item Deleted!");
                            dialog.cancel();
                        }
                    });

            builder.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    public FragmentItems(ActivityListener activityListener) {
        this.activityListener = activityListener;
    }

    public EzloViewModel getEzloViewModel() {
        return ezloViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ezloAdapter = new EzloAdapter(adapterListener);

        ezloViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(AppClass.instance).create(EzloViewModel.class);
        ezloViewModel.init();
        activityListener.showProgress();
        ezloViewModel.getEzloLiveData().observe(this, new Observer<Ezlo>() {
            @Override
            public void onChanged(Ezlo ezlo) {
                if (ezlo != null) {
                    ezloAdapter.setDeviceList(ezlo.getDevices());
                } else {
                    activityListener.showToast("Unable to fetch data");
                }
                activityListener.hideProgress();
            }
        });

        ezloViewModel.getDevices();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_items, container, false);
        View view = binding.getRoot();

        binding.deviceRv.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.deviceRv.setAdapter(ezloAdapter);

        return view;
    }


}
